class TestBugG3 {
      public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f(){
	boolean x, y;
	x = false; // correct initialisation
        if (y) { // y might be incorrectly initialised to true
            return 1;
        } else {
            return 0; // expected behaviour if y is correctly not initialised
        }
    }
}


